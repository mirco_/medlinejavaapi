package de.ocrim.medlineapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.web.client.RestTemplate;

import de.ocrim.medlineapi.model.dto.MedLineAPIResultElement;
import de.ocrim.medlineapi.model.entrez.ELinkResult;
import de.ocrim.medlineapi.model.entrez.article.FullArticle;
import de.ocrim.medlineapi.model.epmc.MedLineResult;
import de.ocrim.medlineapi.model.epmc.Result;
import de.ocrim.medlineapi.model.helper.Helper;

/**
 * Created by mjosefiok on 29.01.15.
 *
 * This class is the main API endpoint and contains all methods for accessing
 * the API
 */
public class MedLineAPIEndpoint {

	/**
	 * Simple API endpoint which returns a list of MedLineAPIResultElement
	 * objects It returns only open access articles
	 * 
	 * @param searchTerm 	The search term, most likely a disease
	 * @return 				A list of MedLineAPIResultElement
	 */
	public Optional<List<MedLineAPIResultElement>> getMedLineApiResult(String searchTerm) {

		RestTemplate restTemplate = new RestTemplate();
		List<MedLineAPIResultElement> medLineAPIResultElements = new ArrayList<MedLineAPIResultElement>();

		// First call against the european pub med rest api for acquiring a list of documents and corresponding metadata as a json
		MedLineResult medLineResult = restTemplate.getForObject("http://www.ebi.ac.uk/europepmc/webservices/rest/search/query=" + searchTerm + " open_access:y&synonym=true&resulttype=lite&format=json", MedLineResult.class);

		for (Result result : medLineResult.getResultList().getResult()) {
			// Second call for acquiring the url for individual documents as a xml
			ELinkResult eLinkResult = restTemplate.getForObject("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&id=" + result.getId() + "&cmd=prlinks", ELinkResult.class);
			// Third call to get the fulltext as a xml
			FullArticle fullArticle = new Helper().getArticle(result.getPmcid());

			medLineAPIResultElements.add(new Helper().getMedLineAPIResultElement(result, fullArticle, eLinkResult));
		}
		return Optional.of(medLineAPIResultElements);
	}
	

	/**
	 * API endpoint which returns a list of MedLineAPIResultElement
	 * 
	 * @param searchTerm 	The search term, most likely a disease
	 * @param openAccess	Flag, whether to return only open access articles
	 * @param synonym		Flag, whether to search for synonyms as well
	 * @return 				A list of MedLineAPIResultElement
	 */
	public Optional<List<MedLineAPIResultElement>> getMedLineApiResult(String searchTerm, Boolean openAccess, Boolean synonym) {

		RestTemplate restTemplate = new RestTemplate();
		List<MedLineAPIResultElement> medLineAPIResultElements = new ArrayList<MedLineAPIResultElement>();
		String open_access = "n";
		
		if(openAccess) {
			open_access = "y";
		}

		// First call against the european pub med rest api for acquiring a list of documents and corresponding metadata as a json
		MedLineResult medLineResult = restTemplate.getForObject("http://www.ebi.ac.uk/europepmc/webservices/rest/search/query=" + searchTerm + " open_access:"+ open_access +"&synonym="+ synonym + "&resulttype=lite&format=json", MedLineResult.class);

		for (Result result : medLineResult.getResultList().getResult()) {
			// Second call for acquiring the url for individual documents as a xml
			ELinkResult eLinkResult = restTemplate.getForObject("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&id=" + result.getId() + "&cmd=prlinks", ELinkResult.class);
			// Third call to get the fulltext as a xml
			FullArticle fullArticle = new Helper().getArticle(result.getPmcid());

			medLineAPIResultElements.add(new Helper().getMedLineAPIResultElement(result, fullArticle, eLinkResult));
		}
		return Optional.of(medLineAPIResultElements);
	}
}