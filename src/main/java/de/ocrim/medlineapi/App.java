package de.ocrim.medlineapi;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import de.ocrim.medlineapi.model.dto.MedLineAPIResultElement;

/**
 * Hello world!
 */
public class App {


	public static void main(String[] args) throws ParserConfigurationException, IOException {
		Optional<List<MedLineAPIResultElement>> medLineAPIResultElementList;
		MedLineAPIEndpoint medLineAPIEndpoint = new MedLineAPIEndpoint();

			
		medLineAPIResultElementList = medLineAPIEndpoint.getMedLineApiResult("vaskulitis", true, true);

		if (medLineAPIResultElementList.isPresent()) {
			System.out.println(medLineAPIResultElementList.get().toString());
		}

	}
}
