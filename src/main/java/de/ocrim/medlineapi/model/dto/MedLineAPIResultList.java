package de.ocrim.medlineapi.model.dto;

import java.util.List;

/**
 * Created by mjosefiok on 29.01.15.
 */
public class MedLineAPIResultList {

    private List<MedLineAPIResultElement> medLineAPIResultElements;

    public List<MedLineAPIResultElement> getMedLineAPIResultElements() {
        return medLineAPIResultElements;
    }

    public void setMedLineAPIResultElements(List<MedLineAPIResultElement> medLineAPIResultElements) {
        this.medLineAPIResultElements = medLineAPIResultElements;
    }
}
