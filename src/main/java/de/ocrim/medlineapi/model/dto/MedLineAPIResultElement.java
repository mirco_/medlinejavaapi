package de.ocrim.medlineapi.model.dto;

/**
 * Created by mjosefiok on 29.01.15.
 */
public class MedLineAPIResultElement {

    private int id;
    private int pmid;
    private String pmcid;
    private String title;
    private String author;
    private String journal;
    private String pubYear;
    private String url;
    private String summary;
    private String authorsummary;

    public String getAuthorsummary() {
        return authorsummary;
    }

    public void setAuthorsummary(String authorsummary) {
        this.authorsummary = authorsummary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public String getPmcid() {
        return pmcid;
    }

    public void setPmcid(String pmcid) {
        this.pmcid = pmcid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getPubYear() {
        return pubYear;
    }

    public void setPubYear(String pubYear) {
        this.pubYear = pubYear;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
