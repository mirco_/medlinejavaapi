package de.ocrim.medlineapi.model.entrez;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mjosefiok on 29.01.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class IdUrlSet {

    @XmlElement(name = "Id")
    private String id;
    @XmlElement(name = "ObjUrl")
    private ObjUrl objUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ObjUrl getObjUrl() {
        return objUrl;
    }

    public void setObjUrl(ObjUrl objUrl) {
        this.objUrl = objUrl;
    }
}
