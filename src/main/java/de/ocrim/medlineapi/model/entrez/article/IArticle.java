package de.ocrim.medlineapi.model.entrez.article;

import java.util.List;

import org.xmlbeam.annotation.XBRead;

public interface IArticle {

	@XBRead("./@article-type")
	String getType();

	@XBRead("./front/article-meta/abstract[@abstract-type='summary']")
	String getAuthorSummary();

	@XBRead("./front/article-meta/abstract/p")
	List<String> getAllAbstracts();
}
