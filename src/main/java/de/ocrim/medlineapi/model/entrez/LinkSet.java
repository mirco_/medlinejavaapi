package de.ocrim.medlineapi.model.entrez;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjosefiok on 29.01.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class LinkSet {

    @XmlElement(name = "DbFrom")
    private String dbFrom;
    @XmlElementWrapper(name="IdUrlList")
    @XmlElement(name = "IdUrlSet")
    private List<IdUrlSet> idUrlList = new ArrayList<IdUrlSet>();

    public String getDbFrom() {
        return dbFrom;
    }

    public void setDbFrom(String dbFrom) {
        this.dbFrom = dbFrom;
    }

    public List<IdUrlSet> getIdUrlList() {
        return idUrlList;
    }

    public void setIdUrlList(List<IdUrlSet> idUrlList) {
        this.idUrlList = idUrlList;
    }
}
