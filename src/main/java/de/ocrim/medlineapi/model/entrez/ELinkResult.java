package de.ocrim.medlineapi.model.entrez;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mjosefiok on 29.01.15.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ELinkResult {


    @XmlElement(name = "LinkSet")
    private LinkSet linkSet;

    public ELinkResult() {
    }


    public LinkSet getLinkSet() {
        return linkSet;
    }

//    public void setLinkSet(LinkSet linkSet) {
//        this.linkSet = linkSet;
//    }
}
