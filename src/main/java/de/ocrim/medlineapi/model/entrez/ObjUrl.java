package de.ocrim.medlineapi.model.entrez;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by mjosefiok on 29.01.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjUrl {

    @XmlElement(name = "Url")
    private String url;
    @XmlElement(name = "IconUrl")
    private String iconUrl;
    @XmlElement(name = "Provider")
    private Provider provider;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
