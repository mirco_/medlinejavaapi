package de.ocrim.medlineapi.model.entrez;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by mjosefiok on 29.01.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Provider {

    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "Url")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
