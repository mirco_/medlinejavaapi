package de.ocrim.medlineapi.model.helper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xmlbeam.XBProjector;
import org.xmlbeam.config.DefaultXMLFactoriesConfig;
import org.xmlbeam.config.XMLFactoriesConfig;

import de.ocrim.medlineapi.model.dto.MedLineAPIResultElement;
import de.ocrim.medlineapi.model.entrez.ELinkResult;
import de.ocrim.medlineapi.model.entrez.article.FullArticle;
import de.ocrim.medlineapi.model.entrez.article.IArticle;
import de.ocrim.medlineapi.model.epmc.Result;

/**
 * Created by mjosefiok on 31.01.15.
 *
 * This class is contains various helper methods
 */
public class Helper {

	private static final XMLFactoriesConfig nonValidatingConfig = new DefaultXMLFactoriesConfig() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public DocumentBuilderFactory createDocumentBuilderFactory() {
			DocumentBuilderFactory factory = super.createDocumentBuilderFactory();
			try {
				factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			} catch (ParserConfigurationException e) {
				throw new RuntimeException(e);
			}
			return factory;
		}
	};

	/**
	 * 
	 * @param strURL
	 *            This should be an URL as a string
	 * @return an xml file as string
	 * @throws Exception
	 */
	public static String retrieveXmlFromUrl(String strURL) {
		String responseXML = null;

		try {
			URL url = new URL(strURL);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;

			// Set the appropriate HTTP parameters.
			// httpConn.setRequestProperty("Content-Length",
			// String.valueOf(requestXML.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestMethod("GET");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Read the response and write it to standard out.
			InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String temp;
			String tempResponse = "";

			// Create a string using response from web services
			while ((temp = br.readLine()) != null)
				tempResponse = tempResponse + temp;
			responseXML = tempResponse;
			br.close();
			isr.close();
		} catch (java.net.MalformedURLException e) {
			System.out.println("Error in postRequest(): Secure Service Required");
		} catch (Exception e) {
			System.out.println("Error in postRequest(): " + e.getMessage());
		}
		return responseXML;
	}

	/**
	 * 
	 * @param result
	 *            A result element of a MedLineResult}
	 * @param fullArticle
	 *            A FullArticle object
	 * @param eLinkResult
	 *            Finally an ELinkResult which should contain the url to the
	 *            full article
	 * @return MedLineAPIResultElement
	 */
	public MedLineAPIResultElement getMedLineAPIResultElement(Result result, FullArticle fullArticle, ELinkResult eLinkResult) {

		// Set properties of the DTO
		MedLineAPIResultElement medLineAPIResultElement = new MedLineAPIResultElement();
		medLineAPIResultElement.setId(Integer.parseInt(result.getId()));
		medLineAPIResultElement.setPmid(Integer.parseInt(result.getPmid()));
		medLineAPIResultElement.setPmcid(result.getPmcid());
		medLineAPIResultElement.setTitle(result.getTitle());
		medLineAPIResultElement.setAuthor(result.getAuthorString());
		medLineAPIResultElement.setJournal(result.getJournalTitle());
		medLineAPIResultElement.setPubYear(result.getPubYear());
		medLineAPIResultElement.setSummary(fullArticle.getSummary());
		medLineAPIResultElement.setAuthorsummary(fullArticle.getAuthorSummary());

		// Sadly not all articels marked with open access have an url
		if (eLinkResult.getLinkSet().getIdUrlList().get(0).getObjUrl() != null) {
			medLineAPIResultElement.setUrl(eLinkResult.getLinkSet().getIdUrlList().get(0).getObjUrl().getUrl());
		} else {
			medLineAPIResultElement.setUrl("#");
		}
		return medLineAPIResultElement;
	}

	/**
	 * 
	 * @param pmcid
	 * @return FullArticle
	 */
	public FullArticle getArticle(String pmcid) {
		FullArticle fullArticle = new FullArticle();
		XBProjector projector = new XBProjector(nonValidatingConfig);
		List<IArticle> articles = projector.io().url("http://www.ebi.ac.uk/europepmc/webservices/rest/" + pmcid + "/fullTextXML").evalXPath("/article").asListOf(IArticle.class);

		for (IArticle article : articles) {
			System.out.println("Type: " + article.getType());
			System.out.println("AuthorSummary: " + article.getAuthorSummary());
			System.out.println("All Abstracts: " + article.getAllAbstracts().get(0).toString());
			fullArticle.setType(article.getType());
			fullArticle.setSummary(article.getAllAbstracts().get(0).toString());
			// Author summary is not mandatory
			if (article.getAuthorSummary() == null) {
				fullArticle.setAuthorSummary("Not set");
			} else {
				fullArticle.setAuthorSummary(article.getAuthorSummary());
			}

		}

		return fullArticle;
	};

	public static XMLFactoriesConfig getNonvalidatingconfig() {
		return nonValidatingConfig;
	}

}
